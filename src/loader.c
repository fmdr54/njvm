#include "loader.h"

unsigned int numberOfInstructions;
unsigned int numberOfSDAVars;

unsigned int *progMemory;
ObjRef *SDA;

void start(void)
{
    loadProgram();
    allocateStackMemory();
    allocateHeap();

    if (arguments->debug)
    {
        printDebugger();
        return;
    }
    executeProgram();
}

void loadProgram(void)
{
    FILE *file;
    if ((file = fopen(arguments->path, "r")) == NULL)
    {
        printError("Could not open file %s", arguments->path);
    }
    handleIdentifier(file);
    handleVersionNumber(file);
    handleNumberOfInstructions(file);
    handleNumberOfSDAVars(file);
    handleProgMemory(file);
    fclose(file);
}

void handleIdentifier(FILE *file)
{
    char identifier[4];
    if (fread(identifier, sizeof(char), 4, file) != 4)
    {
        printError("Could not read identifier");
    }
    if (strncmp(identifier, "NJBF", 4) != 0)
    {
        printError("File is not in NJBF format. Read bytes are: %s", identifier);
    }
}

void handleVersionNumber(FILE *file)
{
    unsigned int versionNumber = -1;
    if (fread(&versionNumber, sizeof(unsigned int), 1, file) != 1)
    {
        printError("Could not read version number");
    }
#if !SKIP_VERSION_CHECK
    if (versionNumber != VERSION)
    {
        printError("files version %d does not match VMs version %d", versionNumber, VERSION);
    }
#endif
}

void handleNumberOfInstructions(FILE *file)
{
    numberOfInstructions = 0;
    if (fread(&numberOfInstructions, sizeof(unsigned int), 1, file) != 1)
    {
        printError("could not read number of instructions");
    }
}

void handleNumberOfSDAVars(FILE *file)
{
    numberOfSDAVars = 0;
    if (fread(&numberOfSDAVars, sizeof(unsigned int), 1, file) != 1)
    {
        printError("could not read number of SDA");
    }
    if ((SDA = malloc(numberOfSDAVars * sizeof(ObjRef))) == NULL)
    {
        printError("SDA malloc");
    }
}

void handleProgMemory(FILE *file)
{
    if ((progMemory = malloc(sizeof(unsigned int) * numberOfInstructions)) == NULL)
    {
        printError("could not allocate memory for prog memory");
    }
    if (fread(progMemory, sizeof(unsigned int), numberOfInstructions, file) != numberOfInstructions)
    {
        printError("could not read instructions");
    }
}