#ifndef _NJVM_H_
#define _NJVM_H_

#define VERSION 8

#include "util.h"
#include "loader.h"
#include "gc.h"
#include "stack.h"

#include <argp.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

typedef struct
{
    bool debug;
    bool gcstats;
    bool gcpurge;
    unsigned int stackSize;
    unsigned int heapSize;
    char *path;
} Arguments;

extern Arguments *arguments;
void freeMemory(void);

#endif