#include "instructions.h"

bool running = true;
unsigned int pc = 0;
unsigned int breakpoint = 0;
ObjRef RVR;

void executeProgram(void)
{
    printStart();
    do
    {
        unsigned int instruction = progMemory[pc];
        pc++;
        execute(instruction);
        if (arguments->debug && breakpoint != -1 && breakpoint == pc)
        {
            return;
        }
    } while (running);
    printEnd();
}

void execute(unsigned int instruction)
{
    unsigned int opcode = GET_OPCODE(instruction);
    int immediate = GET_IMMEDIATE(instruction);

    ObjRef o;
    ObjRef comp;
    int size;

    switch (opcode)
    {
    case HALT:
        running = false;
        break;
    case PUSHC:
        bigFromInt(immediate);
        pushObj((ObjRef)bip.res);
        bip.res = NULL;
        break;
    case ADD:
    case SUB:
    case MUL:
    case DIV:
    case MOD:
        executeArith(opcode, immediate);
        break;
    case RDINT:
        bigRead(stdin);
        pushObj(bip.res);
        bip.res = NULL;
        break;
    case WRINT:
        bip.op1 = popObj();
        bigPrint(stdout);
        bip.op1 = NULL;
        break;
    case RDCHR:
        bigFromInt(fgetc(stdin));
        pushObj(bip.res);
        bip.res = NULL;
        break;
    case WRCHR:
        bip.op1 = popObj();
        printf("%c", (char)bigToInt());
        bip.op1 = NULL;
        break;
    case PUSHG:
        checkSDARange(immediate);
        pushObj(SDA[immediate]);
        break;
    case POPG:
        checkSDARange(immediate);
        SDA[immediate] = popObj();
        break;
    case ASF:
        if (immediate < 0)
        {
            printError("asf size cant be negative");
        }
        pushInt(fp);
        fp = sp;
        sp = sp + immediate;
        nullArea(stack + fp, (sp - fp) * sizeof(StackSlot));
        checkBounds();
        break;
    case RSF:
        sp = fp;
        fp = popInt();
        break;
    case PUSHL:
        checkObj(stack[fp + immediate]);
        push(stack[fp + immediate]);
        break;
    case POPL:
        stack[fp + immediate] = pop();
        checkObj(stack[fp + immediate]);
        break;
    case EQ:
    case NE:
    case LT:
    case LE:
    case GT:
    case GE:
        executeComparison(opcode);
        break;
    case JMP:
        checkJumpTarget(immediate);
        pc = immediate;
        break;
    case BRF:
        if (!popBoolean())
        {
            checkJumpTarget(immediate);
            pc = immediate;
        }
        break;
    case BRT:
        if (popBoolean())
        {
            checkJumpTarget(immediate);
            pc = immediate;
        }
        break;
    case CALL:
        checkJumpTarget(immediate);
        pushInt(pc);
        pc = immediate;
        break;
    case RET:
        pc = popInt();
        break;
    case DROP:
        if (immediate < 0)
        {
            printError("cannot drop negative numbers");
        }
        sp = sp - immediate;
        checkBounds();
        nullArea(stack + sp, immediate);
        break;
    case PUSHR:
        pushObj(RVR);
        break;
    case POPR:
        RVR = popObj();
        break;
    case DUP:
        push(stack[sp - 1]);
        break;
    case NEW:
        if (immediate < 0)
        {
            printError("Compound Obj cannot have negative length");
        }
        pushObj(newCompoundObject(immediate));
        break;
    case GETF:
        o = popObj();
        if (!IS_COMPOUND(o))
        {
            printError("expected compound object");
        }
        size = MASK_SIZE(o->size);
        if (immediate < 0 || immediate >= size)
        {
            printError("index out of range, i: %d, size: %d", immediate, size);
        }
        pushObj(((ObjRef *)o->data)[immediate]);
        break;
    case PUTF:
        // value
        o = popObj();
        // compound Object
        comp = popObj();
        if (!IS_COMPOUND(comp))
        {
            printError("expected compound object");
        }
        size = MASK_SIZE(comp->size);
        if (immediate < 0 || immediate >= size)
        {
            printError("index out of range, i: %d, size: %d", immediate, size);
        }
        ((ObjRef *)comp->data)[immediate] = o;
        break;
    case NEWA:
        bip.op1 = popObj();
        size = bigToInt();
        if (size < 0)
        {
            printError("Compound Obj cannot have negative length");
        }
        pushObj(newCompoundObject(size));
        bip.op1 = NULL;
        break;
    case GETFA:
        bip.op1 = popObj();
        size = bigToInt();
        o = popObj();
        if (!IS_COMPOUND(o))
        {
            printError("expected compound object");
        }
        if (size < 0 || size > MASK_SIZE(o->size))
        {
            printError("index out of range, i: %d, size: %d", size, MASK_SIZE(o->size));
        }
        pushObj(((ObjRef *)o->data)[size]);
        bip.op1 = NULL;
        break;
    case PUTFA:
        // value
        o = popObj();
        // index
        bip.op1 = popObj();
        size = bigToInt();
        // compound Object
        comp = popObj();
        if (!IS_COMPOUND(comp))
        {
            printError("expected compound object");
        }
        if (size < 0 || size > MASK_SIZE(comp->size))
        {
            printError("index out of range, i: %d, size: %d", size, MASK_SIZE(comp->size));
        }
        ((ObjRef *)comp->data)[size] = o;
        bip.op1 = NULL;
        break;
    case GETSZ:
        o = popObj();
        bigFromInt(o != NULL && IS_COMPOUND(o) ? MASK_SIZE(o->size) : -1);
        pushObj(bip.res);
        bip.res = NULL;
        break;
    case PUSHN:
        pushObj(NULL);
        break;
    case REFEQ:
        bip.op2 = popObj();
        bip.op1 = popObj();
        pushBoolean(bip.op1 == bip.op2);
        bip.op1 = NULL;
        bip.op2 = NULL;
        break;
    case REFNE:
        bip.op2 = popObj();
        bip.op1 = popObj();
        pushBoolean(bip.op1 != bip.op2);
        bip.op1 = NULL;
        bip.op2 = NULL;
        break;
    default:
        printError("unknown opcode %d", opcode);
        break;
    }
}

void executeArith(unsigned int opcode, int immediate)
{
    bip.op2 = popObj();
    bip.op1 = popObj();
    switch (opcode)
    {
    case ADD:
        bigAdd();
        break;
    case SUB:
        bigSub();
        break;
    case MUL:
        bigMul();
        break;
    case MOD:
    case DIV:
        bigDiv();
        break;
    }
    pushObj(opcode == MOD ? bip.rem : bip.res);
    bip.op1 = NULL;
    bip.op2 = NULL;
}

void executeComparison(unsigned int opcode)
{
    bip.op2 = popObj();
    bip.op1 = popObj();
    bool result = false;
    switch (opcode)
    {
    case EQ:
        result = bigCmp() == 0 ? true : false;
        break;
    case NE:
        result = bigCmp() != 0 ? true : false;
        break;
    case LT:
        result = bigCmp() < 0 ? true : false;
        break;
    case LE:
        result = bigCmp() <= 0 ? true : false;
        break;
    case GT:
        result = bigCmp() > 0 ? true : false;
        break;
    case GE:
        result = bigCmp() >= 0 ? true : false;
        break;
    }
    pushBoolean(result);
    bip.op1 = NULL;
    bip.op2 = NULL;
}

void checkSDARange(int i)
{
    if (i < 0 || i >= numberOfSDAVars)
    {
        printError("index out of range of SDA: %d", i);
    }
}

void checkJumpTarget(int i)
{
    if (i < 0 || i >= numberOfInstructions)
    {
        printError("invalid jump address: %d", i);
    }
}