#ifndef _GC_H_
#define _GC_H_

#include "njvm.h"
#include "stack.h"

#define SET_BROKEN_HEART(object, offset) ((object)->size = (((IS_COMPOUND((object)) ? 1 : 0) << 31) | (1 << 30) | MASK_SIZE((offset))))
#define IS_BROKEN_HEART(object) (((object)->size & 0x4fffffff) >> 30)

extern void *heap;

void allocateHeap(void);
void *alloc(unsigned int size);
ObjRef relocate(ObjRef obj);
void collectGarbage(void);
void swap(void);
void copyRootObjects(void);
void scan(void);

#endif