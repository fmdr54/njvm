#ifndef _LOADER_H
#define _LOADER_H

#include "util.h"
#include "njvm.h"
#include "instructions.h"
#include "stack.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#define SKIP_VERSION_CHECK true

extern unsigned int numberOfInstructions;
extern unsigned int numberOfSDAVars;

extern ObjRef *SDA;
extern unsigned int *progMemory;

void start(void);
void loadProgram(void);
void handleIdentifier(FILE *file);
void handleVersionNumber(FILE *file);
void handleNumberOfInstructions(FILE *file);
void handleNumberOfSDAVars(FILE *file);
void handleProgMemory(FILE *file);

#endif