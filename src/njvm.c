#include "njvm.h"

const char *argp_program_bug_address = "coding@flodoerr.com";
static const char doc[] = "Ninja Virtual Machine";
static const char args_doc[] = "FILENAME";
static struct argp_option options[] = {
    {"version", 'v', 0, OPTION_ARG_OPTIONAL, "show version and exit"},
    {"debug", 'd', 0, OPTION_ARG_OPTIONAL, "start virtual machine in debug mode"},
    {"gcstats", 'g', 0, OPTION_ARG_OPTIONAL, "show garbage collection statistics"},
    {"gcpurge", 'p', 0, OPTION_ARG_OPTIONAL, "purge old objects after collection"},
    {"stack", 's', "<n>", OPTION_ARG_OPTIONAL, "set stack size to n KBytes (default: n = 64)"},
    {"heap", 'h', "<n>", OPTION_ARG_OPTIONAL, "set heap size to n KBytes (default: n = 8192)"},
    {0}};

Arguments *arguments;

static error_t parse_opt(int key, char *arg, struct argp_state *state)
{
    switch (key)
    {
    case 'v':
        printVersion();
        exit(0);
    case 'd':
        arguments->debug = true;
        return 0;
    case 'g':
        arguments->gcstats = true;
        return 0;
    case 'p':
        arguments->gcpurge = true;
        return 0;
    case 's':
        arguments->stackSize = atoi(arg);
        return 0;
    case 'h':
        arguments->heapSize = atoi(arg);
        return 0;
    case ARGP_KEY_ARG:
        arguments->path = arg;
        return 0;
    default:
        return ARGP_ERR_UNKNOWN;
    }
    return 0;
}

struct argp argp = {options, parse_opt, args_doc, doc, 0, 0, 0};
int main(int argc, char *argv[])
{
    arguments = malloc(sizeof(Arguments));
    arguments->debug = false;
    arguments->gcstats = false;
    arguments->gcpurge = false;
    arguments->stackSize = 64;
    arguments->heapSize = 8192;

    argp_parse(&argp, argc, argv, 0, 0, arguments);

    start();
    freeMemory();
    return 0;
}

void freeMemory(void)
{
    free(heap);
    free(stack);
    free(SDA);
    free(arguments);
}
