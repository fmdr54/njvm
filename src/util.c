#include "util.h"

void println(const char *fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    vfprintf(stdout, fmt, args);
    printf("\n");
    va_end(args);
}

void printDebug(const char *fmt, ...)
{
    printf("DEBUG: ");
    va_list args;
    va_start(args, fmt);
    vfprintf(stdout, fmt, args);
    printf("\n");
    va_end(args);
}

void printError(const char *fmt, ...)
{
    printf("ERROR: ");
    va_list args;
    va_start(args, fmt);
    vfprintf(stdout, fmt, args);
    printf("\n");
    printf("Aborted while executing instruction #%d", pc);
    printf("\n");
    va_end(args);
    abort();
}

void printStart(void)
{
    println("Ninja Virtual Machine started");
}

void printEnd(void)
{
    println("Ninja Virtual Machine stopped");
}

void printVersion(void)
{
    println("Ninja Virtual Machine version %d (compiled %s, %s)", VERSION, __DATE__, __TIME__);
}

void checkObj(StackSlot slot)
{
    if (!slot.isObjRef)
    {
        printError("expected objRef but got number");
    }
}

void checkNumber(StackSlot slot)
{
    if (slot.isObjRef)
    {
        printError("expected number but got objRef");
    }
}

bool getBoolean(ObjRef o)
{
    bip.op1 = o;
    int number = bigToInt();
    if (number != 0 && number != 1)
    {
        printError("expected boolean got: %d", number);
    }
    return number == 1 ? true : false;
}

unsigned int getSize(ObjRef obj)
{
    if (IS_COMPOUND(obj))
    {
        return sizeof(unsigned int) + obj->size * sizeof(ObjRef);
    }
    return sizeof(unsigned int) + obj->size;
}

void nullArea(void *ptr, int size)
{
    memset(ptr, 0, size);
}

void *newPrimObject(int dataSize)
{
    if (dataSize == 0)
    {
        abort();
    }
    int size = sizeof(unsigned int) +
               dataSize * sizeof(unsigned char);
    ObjRef bigObjRef = alloc(size);
    if (bigObjRef == NULL)
    {
        printError("newPrimObject() got no memory");
    }
    nullArea(bigObjRef, size);
    bigObjRef->size = MASK_SIZE(dataSize);
    SET_IS_PRIMITIVE(bigObjRef);
    return bigObjRef;
}

ObjRef newCompoundObject(unsigned int numObjRefs)
{
    int size = sizeof(unsigned int) +
               numObjRefs * sizeof(ObjRef);
    ObjRef obj = alloc(size);
    if (obj == NULL)
    {
        printError("newPrimObject() got no memory");
    }
    nullArea(obj, size);
    obj->size = MASK_SIZE(numObjRefs);
    SET_IS_COMPOUND(obj);
    return obj;
}

void *getPrimObjectDataPointer(void *obj)
{
    ObjRef oo = ((ObjRef)(obj));
    return oo->data;
}

void fatalError(char *msg)
{
    printError("%s", msg);
}

static const char *OPCODES_STRING[] = {FOREACH_OPCODES(GENERATE_STRING)};

/**
 * prints a whole program from the program memory to console
 *
 **/
void printProg(void)
{
    unsigned int instruction;
    unsigned int opcode;
    int immediate;
    for (int temp_pc = 0; temp_pc < numberOfInstructions; temp_pc++)
    {
        instruction = progMemory[temp_pc];
        opcode = GET_OPCODE(instruction);
        immediate = GET_IMMEDIATE(instruction);
        size_t len = strlen(OPCODES_STRING[opcode]);
        char *lower = calloc(len + 1, sizeof(char));
        for (size_t i = 0; i < len; ++i)
        {
            lower[i] = tolower((unsigned char)OPCODES_STRING[opcode][i]);
        }
        if (immediate != 0 || opcode == POPG || opcode == PUSHG)
        {
            printf("%03d:\t%s\t%d\n", temp_pc, lower, immediate);
        }
        else
        {
            printf("%03d:\t%s\n", temp_pc, lower);
        }
    }
    printf("\t--- end of code ---\n");
}

void printDebugger(void)
{

    do
    {
        printInstruction();
        bool correctInput = false;
        do
        {
            printDebugCommands();
            char in = fgetc(stdin);
            unsigned int instruction;
            switch (in)
            {
            case 'i':
                printDebug("[inspect]: stack, data, object?");
                // flush \n
                getchar();

                char input = fgetc(stdin);
                switch (input)
                {
                case 's':
                    printStack();
                    correctInput = true;
                    break;
                case 'd':
                    printSDA();
                    correctInput = true;
                    break;
                case 'o':
                    printf("object reference?\n");
                    // flush \n
                    getchar();
                    void *address;
                    // unsigned long address;
                    // scanf("0x%lx", &address);
                    scanf("%p", &address);
                    // ObjRef value = (ObjRef)(address);

                    bip.op1 = (ObjRef *)address;

                    if (IS_COMPOUND((ObjRef)bip.op1))
                    {
                        println("Object is a compound object containing:");
                        ObjRef *array = (ObjRef *)((ObjRef)bip.op1)->data;
                        int size = MASK_SIZE(((ObjRef)bip.op1)->size);
                        for (int i = 0; i < size; i++)
                        {
                            println("%d.: %p", i, array[i]);
                        }
                    }
                    else
                    {
                        // getValueOfObj();
                        printf("value = ");
                        bigPrint(stdout);
                        printf("\n");
                    }
                    correctInput = true;
                    break;
                default:
                    correctInput = false;
                    break;
                }
                break;
            case 'l':
                printProg();
                correctInput = true;
                break;
            case 'b':
                switch (breakpoint)
                {
                case -1:
                    printDebug("[breakpoint]: cleared");
                    break;
                default:
                    printf("DEBUG: [breakpoint]: %d\n", breakpoint);
                    break;
                }
                printDebug("[breakpoint]: address to set, -1 to clear, <ret> for no change?");
                // flush \n
                getchar();
                int inputbr;
                scanf("%d", &inputbr); // TODO: String besser um auf <ret> besser reagieren zu können? <ret> wird so als 0 gezählt.
                if (inputbr <= 0)
                {
                    if (inputbr == -1)
                    {
                        breakpoint = -1;
                        printDebug("[breakpoint]: breakpoint cleared");
                        correctInput = true;
                    }
                    else
                    {
                        correctInput = false;
                    }
                }
                else
                {
                    breakpoint = inputbr;
                    printf("DEBUG: [breakpoint]: now set to %d\n", breakpoint);
                    correctInput = true;
                }
                break;
            case 's':
                instruction = progMemory[pc];
                pc++;
                execute(instruction);
                correctInput = true;
                break;
            case 'r':
                executeProgram();
                correctInput = true;
                break;
            case 'q':
                printEnd();
                exit(0);
                break;
            default:
                correctInput = false;
                break;
            }
            // flush \n
            getchar();
        } while (!correctInput);

    } while (running);
}

void printInstruction(void)
{
    unsigned int instruction = progMemory[pc];
    unsigned int opcode = GET_OPCODE(instruction);
    int immediate = GET_IMMEDIATE(instruction);
    size_t len = strlen(OPCODES_STRING[opcode]);
    char *lower = calloc(len + 1, sizeof(char));
    for (size_t i = 0; i < len; ++i)
    {
        lower[i] = tolower((unsigned char)OPCODES_STRING[opcode][i]);
    }
    if (immediate != 0 || opcode == POPG || opcode == PUSHG)
    {
        printf("%03d:\t%s\t%d\n", pc, lower, immediate);
    }
    else
    {
        printf("%03d:\t%s\n", pc, lower);
    }
}

void printDebugCommands(void)
{
    printDebug("inspect, list, breakpoint, step, run, quit?");
}

void printSDA(void)
{
    for (int i = 0; i < numberOfSDAVars; i++)
    {
        printf("data[%04d]:\t(objref) %p\n", i, (void *)SDA[i]);
    }
    printf("\t--- end of data ---\n");
}