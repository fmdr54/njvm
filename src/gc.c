#include "gc.h"

void *heap;
unsigned int numberOfGCs = 1;
unsigned long numberOfAllocatedBytes = 0;
unsigned long numberOfObjects = 0;
unsigned long copiedObjects = 0;
unsigned long copiedBytes = 0;

struct
{
    unsigned char *free;
    unsigned int halfSize;
    unsigned char *start;
    unsigned char *active;
} Mem;

void allocateHeap(void)
{
    if ((heap = malloc(arguments->heapSize * 1024)) == NULL)
    {
        printError("could not allocate heap with size of: %d", arguments->heapSize * 1024);
    }
    Mem.halfSize = (arguments->heapSize * 1024) / 2;
    Mem.free = heap;
    Mem.start = heap;
    Mem.active = heap;
}

void *alloc(unsigned int size)
{
    if ((Mem.free + size) >= (Mem.active + Mem.halfSize))
    {
        collectGarbage();
        if ((Mem.free + size) >= (Mem.active + Mem.halfSize))
        {
            printError("out of memory after %d. gc", numberOfGCs);
        }
        numberOfGCs++;
    }
    void *newObj = (void *)Mem.free;
    numberOfAllocatedBytes += size;
    numberOfObjects++;
    Mem.free += size;
    return newObj;
}

ObjRef relocate(ObjRef obj)
{
    if (obj == NULL)
    {
        return NULL;
    }
    if (IS_BROKEN_HEART(obj))
    {
        return ((ObjRef)(Mem.active + MASK_SIZE(obj->size)));
    }
    unsigned int size = getSize(obj);
    ObjRef newObj = (ObjRef)Mem.free;
    memcpy(newObj, obj, size);
    SET_BROKEN_HEART(obj, (unsigned int)((long)newObj - (long)Mem.active));
    copiedObjects++;
    copiedBytes += size;
    Mem.free += size;
    return newObj;
}

void collectGarbage(void)
{
    swap();

    if (arguments->gcpurge)
    {
        memset(Mem.active, 0, Mem.halfSize);
    }

    copyRootObjects();

    scan();

    if (arguments->gcstats)
    {
        println("Garbage Collector (#%d):", numberOfGCs);
        println("\t%ld objects (%ld bytes) allocated since last collection", numberOfObjects, numberOfAllocatedBytes);
        println("\t%ld objects (%ld bytes) copied during this collection", copiedObjects, copiedBytes);
        println("\t%ld of %d bytes free after this collection", Mem.halfSize - copiedBytes, Mem.halfSize);
    }
    numberOfAllocatedBytes = copiedBytes;
    numberOfObjects = copiedObjects;
}

void swap(void)
{
    if (Mem.active == Mem.start)
    {
        Mem.active = Mem.start + Mem.halfSize;
    }
    else
    {
        Mem.active = Mem.start;
    }
    Mem.free = Mem.active;
}

void copyRootObjects(void)
{

    copiedBytes = 0;
    copiedObjects = 0;

    for (int i = 0; i < numberOfSDAVars; i++)
    {
        SDA[i] = relocate(SDA[i]);
    }

    RVR = relocate(RVR);

    int objCounter = 0;
    for (int i = 0; i < sp; i++)
    {
        StackSlot slot = stack[i];
        if (slot.isObjRef)
        {
            ObjRef obj = slot.data.objRef;
            stack[i].data.objRef = relocate(obj);
            objCounter++;
        }
    }

    bip.op1 = relocate(bip.op1);
    bip.op2 = relocate(bip.op2);
    bip.rem = relocate(bip.rem);
    bip.res = relocate(bip.res);
}

void scan(void)
{
    unsigned char *ptr = Mem.active;
    while (ptr < Mem.free)
    {
        ObjRef obj = (ObjRef)ptr;
        if (IS_COMPOUND(obj))
        {
            for (int i = 0; i < MASK_SIZE(obj->size); i++)
            {
                ObjRef currElement = (ObjRef)(((ObjRef *)obj->data)[i]);
                ((ObjRef *)obj->data)[i] = relocate(currElement);
            }
        }
        ptr += getSize(obj);
    }
}