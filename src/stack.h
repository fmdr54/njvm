#ifndef _STACK_H_
#define _STACK_H_

#include <stdbool.h>

#define OBJ_SIZE sizof(unsigned int)

typedef struct Obj
{
    unsigned int size;
    char data[1]; // size as needed
} * ObjRef;

typedef struct
{
    bool isObjRef;
    union
    {
        ObjRef objRef;
        int number;
    } data;
} StackSlot;

#include "util.h"

extern int sp;
extern unsigned int fp;
extern long maxStackSlots;

extern StackSlot *stack;

void allocateStackMemory(void);

void push(StackSlot slot);
void pushInt(int number);
void pushObj(ObjRef obj);
void pushBoolean(bool boolean);

StackSlot pop(void);
int popInt(void);
ObjRef popObj(void);
bool popBoolean(void);

void checkBounds(void);
void printStack(void);

#endif