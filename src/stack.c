#include "stack.h"

int sp = 0;
unsigned int fp = 0;
StackSlot *stack;

long maxStackSlots;

void allocateStackMemory(void)
{
    unsigned int size = arguments->stackSize * 1024;
    maxStackSlots = size / sizeof(StackSlot);
    if ((stack = malloc(size)) == NULL)
    {
        printError("stack malloc");
    }
    nullArea(stack, size);
}

void push(StackSlot slot)
{
    checkBounds();
    stack[sp] = slot;
    sp++;
}

void pushInt(int number)
{
    checkBounds();
    stack[sp].isObjRef = false;
    stack[sp].data.number = number;
    sp++;
}

void pushObj(ObjRef obj)
{
    checkBounds();
    stack[sp].isObjRef = true;
    stack[sp].data.objRef = obj;
    sp++;
}

void pushBoolean(bool boolean)
{
    bigFromInt(boolean);
    pushObj(bip.res);
}

StackSlot pop(void)
{
    sp--;
    checkBounds();
    StackSlot slot = stack[sp];
    return slot;
}

int popInt(void)
{
    StackSlot slot = pop();
    checkNumber(slot);
    return slot.data.number;
}

ObjRef popObj(void)
{
    StackSlot slot = pop();
    checkObj(slot);
    return slot.data.objRef;
}

bool popBoolean(void)
{
    StackSlot slot = pop();
    checkObj(slot);
    bip.op1 = slot.data.objRef;
    int number = bigToInt();
    bip.op1 = NULL;
    if (number != 0 && number != 1)
    {
        printError("expected boolean got: %d", number);
    }
    return number == 1 ? true : false;
}

void checkBounds(void)
{
    if (sp >= maxStackSlots)
    {
        printError("stack overflow. SP: %d, max: %d", sp, maxStackSlots);
    }
    else if (sp < 0)
    {
        printError("stack underflow. SP: %d, max: %d", sp, maxStackSlots);
    }
}

void printStack(void)
{
    for (int i = sp; i >= 0; i--)
    {
        if (sp == fp && sp == i)
        {
            printf("fp, sp\t--->\t%04d:\t(xxxxxx) xxxxxx\n", i);
        }
        else if (sp == i)
        {
            printf("sp\t--->\t%04d:\t(xxxxxx) xxxxxx\n", i);
        }
        else if (fp == i)
        {
            if (stack[i].isObjRef)
            {
                if (stack[i].data.objRef == NULL)
                {
                    printf("fp\t--->\t%04d:\t(objref) nil\n", i);
                }
                else
                {
                    printf("fp\t--->\t%04d:\t(objref) %p\n", i, (void *)stack[i].data.objRef);
                }
            }
            else
            {
                printf("fp\t--->\t%04d:\t(number) %d\n", i, stack[i].data.number);
            }
        }
        else if (stack[i].isObjRef)
        {
            if (stack[i].data.objRef == NULL)
            {
                printf("\t--->\t%04d:\t(objref) nil\n", i);
            }
            else
            {
                printf("\t--->\t%04d:\t(objref) %p\n", i, (void *)stack[i].data.objRef);
            }
        }
        else
        {
            printf("\t--->\t%04d:\t(number) %d\n", i, stack[i].data.number);
        }
    }
    printf("\t\t--- bottom of stack ---\n");
}