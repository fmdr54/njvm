#ifndef _INSTRUCTIONS_H_
#define _INSTRUCTIONS_H_

#include "util.h"
#include "bigint.h"
#include "stack.h"

#include <stdbool.h>

#define FOREACH_OPCODES(OPCODE) \
    OPCODE(HALT)                \
    OPCODE(PUSHC)               \
    OPCODE(ADD)                 \
    OPCODE(SUB)                 \
    OPCODE(MUL)                 \
    OPCODE(DIV)                 \
    OPCODE(MOD)                 \
    OPCODE(RDINT)               \
    OPCODE(WRINT)               \
    OPCODE(RDCHR)               \
    OPCODE(WRCHR)               \
    OPCODE(PUSHG)               \
    OPCODE(POPG)                \
    OPCODE(ASF)                 \
    OPCODE(RSF)                 \
    OPCODE(PUSHL)               \
    OPCODE(POPL)                \
    OPCODE(EQ)                  \
    OPCODE(NE)                  \
    OPCODE(LT)                  \
    OPCODE(LE)                  \
    OPCODE(GT)                  \
    OPCODE(GE)                  \
    OPCODE(JMP)                 \
    OPCODE(BRF)                 \
    OPCODE(BRT)                 \
    OPCODE(CALL)                \
    OPCODE(RET)                 \
    OPCODE(DROP)                \
    OPCODE(PUSHR)               \
    OPCODE(POPR)                \
    OPCODE(DUP)                 \
    OPCODE(NEW)                 \
    OPCODE(GETF)                \
    OPCODE(PUTF)                \
    OPCODE(NEWA)                \
    OPCODE(GETFA)               \
    OPCODE(PUTFA)               \
    OPCODE(GETSZ)               \
    OPCODE(PUSHN)               \
    OPCODE(REFEQ)               \
    OPCODE(REFNE)

#define GENERATE_ENUM(ENUM) ENUM,
#define GENERATE_STRING(STRING) #STRING,

enum Opcodes
{
    FOREACH_OPCODES(GENERATE_ENUM)
};

#define GET_OPCODE(instruction) ((instruction) >> (8 * 3))
#define SIGN_EXTEND(instruction) ((instruction)&0x00800000 ? (instruction) | 0xff000000 : (instruction))
#define GET_IMMEDIATE(instruction) (SIGN_EXTEND((instruction)&0x00ffffff))

#define IS_COMPOUND(object) ((object)->size >> 31 == 1 ? true : false)
#define SET_IS_COMPOUND(object) ((object)->size = (1 << 31 | (object)->size))
#define SET_IS_PRIMITIVE(object) ((object)->size = (~(0 << 31) & (object)->size))
#define MASK_SIZE(number) ((number)&0x3fffffff)
#define GET_SIZE(object) (MASK_SIZE((object)->size)))

extern ObjRef RVR;
extern bool running;
extern unsigned int pc;
extern unsigned int breakpoint;

void executeProgram(void);
void execute(unsigned int instruction);
void executeArith(unsigned int opcode, int immediate);
void executeComparison(unsigned int opcode);
void checkSDARange(int i);
void checkJumpTarget(int i);

#endif