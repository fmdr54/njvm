#ifndef _UTIL_H_
#define _UTIL_H_

#include "njvm.h"
#include "stack.h"
#include "support.h"
#include "gc.h"

#include <stdarg.h>
#include <stdio.h>

void println(const char *fmt, ...);
void printError(const char *fmt, ...);
void printVersion(void);
void printStart(void);
void printEnd(void);
void nullArea(void *ptr, int size);
void checkObj(StackSlot slot);
void checkNumber(StackSlot slot);
bool getBoolean(ObjRef o);
unsigned int getSize(ObjRef obj);

void *newPrimObject(int dataSize);
ObjRef newCompoundObject(unsigned int numObjRefs);
void *getPrimObjectDataPointer(void *obj);
void fatalError(char *msg);

void printProg(void);
void printDebugger(void);
void printInstruction(void);
void printDebugCommands(void);
void printSDA(void);

#endif